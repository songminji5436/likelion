### 미션 요구사항 분석 & 체크리스트

---

### 필수 과제
❌회원가입<br>
☑️Swagger<br>
☑️AWS EC2에 Docker 배포<br>
☑️Gitlab CI & Crontab CD<br>
❌로그인<br>
❌포스트 작성, 수정, 삭제, 리스트

### 도전과제
❌화면 UI 개발<br>
- 회원가입, 로그인, 글쓰기, 조회<br>
❌ADMIN 회원으로 등급업하는 기능<br>
❌초기 ADMIN 회원은 하나가 존재하고 ADMIN 회원은 일반회원의 권한을 ADMIN으로 승격시킬 수 있다.<br>
❌ADMIN 회원이 일반 회원을 ADMIN으로 승격시키는 기능<br>
- POST /users/{id}/role/change<br>
- Body {”role”:”admin” | “user”} admin 또는 user로 변경할 수 있습니다.<br>
❌ADMIN 회원이 로그인 시 자신이 쓴 글이 아닌 글과 댓글에 수정, 삭제를 할 수 있는 기능<br>

<br>

### 1주차 미션 요약

---
**[접근 방법]**

- SWAGGER
    - 구글링을 통해 swagger 관련 문서를 확인하며 작성
    - swagger를 통해 구현한 부분을 확인할 수 있다.
    - 수업시간에 swagger를 작성해보았습니다.
- AWS EC2에 DOCKER 배포
    - AWS EC2 공식 문서를 확인하며 구현
    - EC2에 DOCKER 배포에 성공하였다.
    - 수업시간에 AWS EC2에 DOCKER 배포하는 실습을 해보았습니다.
- Gitlab CI & Crontab CD
    - 커밋을 할 경우 바로 적용되어 이미지를 pull하고 이를 적용시킨 서버가 실행 될 수 있도록 crontab을 설정하였다.
    - Gitlab CI 공식 문서와 구글링을 통해 관련 문서들을 참고하여 구현하였다.
    - 수업시간에 CI/CD를 배웠고 구현해보았습니다.

**[특이사항]**

#### 1. 아쉬웠던 점
- 모든 요구사항을 구현하지 못하였습니다.
    - 추후 리팩토링 시 부족한 부분을 구현
    - 피어리뷰를 통해 받은 의견과 피드백을 통해 수정해볼 것입니다.
#### 2. 궁금했던 점
