package com.example.demo.Controller;

import com.example.demo.Domain.DTO.UserJoinRequest;
import com.example.demo.Domain.DTO.UserLoginRequest;
import com.example.demo.Service.UserService;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/users")
public class UserController {
    private final UserService userService;

    //@Operation(summary = "게시글 조회", description = "id를 이용하여 posts 레코드를 조회합니다.")
    @PostMapping("/join")
    public ResponseEntity<String> join(@RequestBody UserJoinRequest dto){
        userService.join(dto.getUserName(), dto.getPassword());
        Map<String, String> resultcode = new HashMap<>();
        resultcode.put("resultcode", "SUCCESS");
        Map<String, Object> result = new HashMap<>();
        result.put("result", dto.getUserName());
        return ResponseEntity.ok().body("d");
    }
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserLoginRequest dto){
        String token = userService.login(dto.getUserName(), dto.getPassword());
        return ResponseEntity.ok().body(token);
    }
}
