package com.example.demo.Controller;

import io.swagger.annotations.ApiImplicitParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Service.AlgorithmService;

@RestController
@RequestMapping("/api/v1/hello")
public class Hello {
    @GetMapping
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok().body("송민지");
    }

    @ApiImplicitParam(
            name = "num"
            , value = "num"
            , required = true
            , dataType = "integer($int32)"
            , paramType = "path"
            , defaultValue = "None")
    @GetMapping("/{num}")
    public ResponseEntity<Integer> num(@PathVariable(name = "num") int num) {
        return ResponseEntity.ok().body(AlgorithmService.SumOfDigit(num));
    }
}
