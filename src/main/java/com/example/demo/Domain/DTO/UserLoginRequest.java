package com.example.demo.Domain.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class UserLoginRequest {
        private String userName;
        private String password;
}
