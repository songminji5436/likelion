package com.example.demo.Service;

public class AlgorithmService {
    public static int SumOfDigit(int num){
        int sum = 0;
        while(num>0){
            sum += num%10;
            num = num/10;
        }
        return sum;
    }
}
