package com.example.demo.Service;

import com.example.demo.Domain.User;
import com.example.demo.Exception.AppException;
import com.example.demo.Exception.ErrorCode;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    private Long expireTimeMs = 1000 * 60 * 60l;
    @Value("${jwt.token.secret}")
    private String key;
    public String join(String userName, String password){

        //  USERnAME중복체크
        userRepository.findByUserName(userName).ifPresent(user -> {
            throw new AppException(ErrorCode.USERNAME_DUPLICATED, userName + "는 이미 있습니다.");
        });

        // 저장
        User user = User.builder()
                .userName(userName)
                .password(encoder.encode(password))
                .build();
        userRepository.save(user);

        return "SUCCESS";
    }

    public String login(String userName, String password){
        // username 없음
        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOTFOUND, userName + "이 없습니다."));
        // password틀림
        if(!encoder.matches(password, selectedUser.getPassword())){
            throw new AppException(ErrorCode.INVALID_PASSWORD, "패스워드가 잘못입력하였습니다.");
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), key, expireTimeMs);

        // 앞에서 예외 x -> 토큰 발행
        return token;
    }
}
